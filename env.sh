#install tomcat
sudo apt-get install tomcat7 -y

#install mysql & php & phpmyadmin
sudo apt-get install apache2 php5 mysql-server
sudo apt-get install libapache2-mod-php5
sudo apt-get install phpmyadmin

# setting phpmyadmin
cp /etc/phpmyadmin/config.sample.inc.php /etc/phpmyadmin/config.inc.php
echo "include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf

sudo /etc/init.d/apache2 restart
