#update apt
sudo apt-get update

#install tomcat
sudo apt-get install tomcat7

#install mysql & php & phpmyadmin
sudo apt-get install apache2 php5 mysql-server
sudo apt-get install libapache2-mod-php5
sudo apt-get install phpmyadmin

# setting phpmyadmin
cp /etc/phpmyadmin/config.sample.inc.php /etc/phpmyadmin/config.inc.php
echo "include /etc/phpmyadmin/apache.conf" >> /etc/apache2/apache2.conf

#install libreoffice
sudo add-apt-repository ppa:libreoffice/ppa
sudo apt-get update
sudo apt-get install 4.3.3 -y

#ufw open 8100
sudo ufw allow in 8100
sudo ufw allow out 8100

#set libreoffice service
sudo cp libreoffice.sh /etc/init.d/
sudo chmod 0755 /etc/init.d/libreoffice.sh
sudo update-rc.d libreoffice.sh defaults
sudo /etc/init.d/libreoffice.sh start

#big5 setting
sudo apt-get install language-pack-gnome-zh
sudo apt-get install ttf-wqy-zenhei

#paperless
sudo cp paperlessweb.war /var/lib/tomcat7/webapps/
sudo mkdir /var/lib/tomcat7/certs
sudo chmod 777 /var/lib/tomcat7/certs



