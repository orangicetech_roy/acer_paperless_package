# README #

此文件用於描述如何使用此包裹內的檔案安裝系統與更新

### 套件介紹 ###

* 無紙化會議主系統
* 無紙化會議更新檔

### 環境準備 ###

* 要有能上網且作業系統為Ubuntu的主機
* 先進行sudo apt-get update 更新清單
* 透過sudo apt-get install git 安裝Git

![Parallels 圖片 1.png](https://bitbucket.org/repo/KzBXeX/images/2582651000-Parallels%20%E5%9C%96%E7%89%87%201.png)

* 在/~下執行sudo git clone https://orangicetech_roy@bitbucket.org/orangicetech/paperless_installer.git可下載整包的檔案

![Parallels 圖片 2.png](https://bitbucket.org/repo/KzBXeX/images/1824501051-Parallels%20%E5%9C%96%E7%89%87%202.png)
![Parallels 圖片 3.png](https://bitbucket.org/repo/KzBXeX/images/2997014783-Parallels%20%E5%9C%96%E7%89%87%203.png)


### 如何安裝 ###

* 進入剛clone下來的資料夾

![Parallels 圖片 4.png](https://bitbucket.org/repo/KzBXeX/images/332704740-Parallels%20%E5%9C%96%E7%89%87%204.png)

* sudo sh install.sh, 此方式會開始自動的安裝與配置, 中間會需要設定mysql的帳號密碼, 如果有[Y/n]直接選Y
![Parallels 圖片 5.png](https://bitbucket.org/repo/KzBXeX/images/2988485960-Parallels%20%E5%9C%96%E7%89%87%205.png)
![Parallels 圖片 6.png](https://bitbucket.org/repo/KzBXeX/images/1294032877-Parallels%20%E5%9C%96%E7%89%87%206.png)
![Parallels 圖片 7.png](https://bitbucket.org/repo/KzBXeX/images/847582266-Parallels%20%E5%9C%96%E7%89%87%207.png)
* 進入 http://localhost:8080/paperlessweb/ 設定
設定最高權限的帳號密碼

![Parallels 圖片 9.png](https://bitbucket.org/repo/KzBXeX/images/2051029731-Parallels%20%E5%9C%96%E7%89%87%209.png)
設定資料庫的連線資訊並確認

![Parallels 圖片 10.png](https://bitbucket.org/repo/KzBXeX/images/2881887489-Parallels%20%E5%9C%96%E7%89%87%2010.png)
![Parallels 圖片 11.png](https://bitbucket.org/repo/KzBXeX/images/4288266178-Parallels%20%E5%9C%96%E7%89%87%2011.png)
設定電子郵件的相關資訊

![Parallels 圖片 12.png](https://bitbucket.org/repo/KzBXeX/images/1330606812-Parallels%20%E5%9C%96%E7%89%87%2012.png)
![Parallels 圖片 13.png](https://bitbucket.org/repo/KzBXeX/images/437874626-Parallels%20%E5%9C%96%E7%89%87%2013.png)

### 權限調整[非必要] ###

* 第一步驟 
> 首先使用 root 的權限執行 sudo visudo  ( vi /etc/sudoers )
>
> 將最後一行的
>
> %admin ALL=(ALL) ALL
> 改成
>
> %admin ALL=(ALL) NOPASSWD: NOPASSWD: ALL
>
> 存檔退出，即可。

* 第二步驟
> 將使用者加入admin的群組
>
> adduser 目前的帳號 admin
>
> adduser tomcat7 admin

### 如何升級 ###

## 方法一: 下載檔案 ##

* 只適用Ubuntu環境, 必須安裝Ant
* 下載upgrade.sh, upgrade.xml, upgrade.zip
* 執行sudo sh upgrade.sh

## 方法二: Git 安裝 ##

* 只適用Ubuntu環境, 必須安裝Ant

![Parallels 圖片 15.png](https://bitbucket.org/repo/KzBXeX/images/2731949009-Parallels%20%E5%9C%96%E7%89%87%2015.png)

* 進入Git的資料夾, 使用sudo git pull將檔案拉下來
* 執行sudo sh upgrade.sh

![Parallels 圖片 16.png](https://bitbucket.org/repo/KzBXeX/images/719602435-Parallels%20%E5%9C%96%E7%89%87%2016.png)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact