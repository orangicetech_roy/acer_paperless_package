#!/bin/bash
# libreoffice.org  headless server script
#
# chkconfig: 2345 80 30
# description: headless libreoffice server script
# processname: libreoffice
# 
# Author: Vic Vijayakumar
# Modified by Federico Ch. Tomasczik
# Modified by Manuel Vega Ulloa
# Modified by wild0
OOo_HOME=/usr/bin
SOFFICE_PATH=$OOo_HOME/soffice
PIDFILE=/var/run/libreoffice-server.pid
set -e
case "$1" in
    start)
    if [ -f $PIDFILE ]; then
      echo "LibreOffice headless server has already started."
      sleep 5
      exit
    fi
      echo "Starting LibreOffice headless server"
      $SOFFICE_PATH --headless --nologo --nofirststartwizard --accept="socket,host=127.0.0.1,port=8100;urp" & > /dev/null 2>&1
      touch $PIDFILE
    ;;
    stop)
    if [ -f $PIDFILE ]; then
      echo "Stopping LibreOffice headless server."
      killall -q soffice && killall -q soffice.bin
      rm -f $PIDFILE
      exit
    fi
      echo "LibreOffice headless server is not running."
      exit
    ;;
    restart)
        killall -q soffice && killall -q soffice.bin
        rm -f $PIDFILE

        $SOFFICE_PATH --headless --nologo --nofirststartwizard --accept="socket,host=127.0.0.1,port=8100;urp" & > /dev/null 2>&1
      touch $PIDFILE
        echo "LibreOffice have been restart."
        exit
    ;;
    *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac
exit 0
